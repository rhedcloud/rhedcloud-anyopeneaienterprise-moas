@echo off

echo OpenEAI Example Enterprise Start System
echo ---------------------------------------
echo.

set OPENEAI_HOME=./

if "%JAVA_HOME%" == "" goto error
if "%OPENEAI_HOME%" == "" goto openeai_error
if "%1%" == "" goto noparms

rem set OPENEAI_LIB=%OPENEAI_HOME%\lib
set OPENEAI_RUNTIME=%OPENEAI_HOME%\configs\messaging\Environments\Examples\
set BUILD_FILE=%OPENEAI_HOME%\build.xml
rem set LOCALCLASSPATH=%JAVA_HOME%\lib\tools.jar;%OPENEAI_LIB%\ant.jar;%OPENEAI_CLASSPATH%
set LOCALCLASSPATH=%JAVA_HOME%\lib\tools.jar;%OPENEAI_HOME%\lib\ant.jar
rem set ANT_HOME=%OPENEAI_LIB%
set ANT_HOME=%OPENEAI_HOME%\lib

title %1
echo Starting %1%
echo.

echo Starting Ant...

"%JAVA_HOME%\bin\java.exe" -Xms96m -Xmx384m -Dant.home="%ANT_HOME%" -classpath "%LOCALCLASSPATH%" org.apache.tools.ant.Main -buildfile %BUILD_FILE% %1 %2 %3 %4 %5

goto end

:noparms

echo ERROR: Invalid number of arguments.
echo Please provide a 'target' that you wish to start.
echo Targets are specified in the build.xml file.
goto end

:error

echo ERROR: JAVA_HOME not found in your environment.
echo Please, set the JAVA_HOME variable in your environment to match the
echo location of the Java Virtual Machine you want to use.
goto end

:openeai_error

echo ERROR: OPENEAI_HOME not found in your environment.
echo Please, set the OPENEAI_HOME variable in your environment to match the
echo location of the OpenEAI Examples installation directory you want to use.

:end
